        window.onload=function(){
            var winW=document.documentElement.clientWidth||document.body.clientWidth;
            var winH=document.documentElement.clientHeight||document.body.clientHeight;
            var suanshi=document.getElementById("算式");
            var result=document.getElementById("结果");
            suanshi.style.width=winW;
            result.style.width=winW;
        }
        //定义点击按钮的输入事件
        function input(id){
            document.getElementById("算式").innerHTML+=id.innerHTML;
        }
        //定义无括号算式的运算方法
        function innercaculate(str){
            var num1=0;
            var num2=0;
            var num3=0;
            var numbers=new Array();
            var Strnumbers=new Array();
            var operators=new Array();
            var replaceNums=new Array();
            var replaceStrings=new Array();
            var numbers1=new Array();
            var operators1=new Array();
            var temp="";
            var result=0;
            var operatorNum=0;
            //算式的第一个字符不能是操作符和")"
            if(isOperator(str.charAt(0))==true||str.charAt(0)==")"){
                return "Error!";
            }
            //储存操作符、数字
            //新的数组元素类型要先定义成字符串类型
            for(var i=0;i<str.length;i++){
                temp=str.charAt(i);
                if(i==0){
                    numbers[num1]=temp;
                }
                if(i>0){
                    var previousChar=str.charAt(i-1);
                    if(isNum(temp)==true||temp=="."){
                        if(isOperator(previousChar)==true){
                            numbers[num1]=temp;
                        }
                        if(isNum(previousChar)==true||previousChar=="."){
                            numbers[num1]+=temp;
                        }
                    }
                    if(isOperator(temp)==true){
                        if(isOperator(previousChar)==true){
                            return "Error!";
                        }
                        operators[num1]=temp;
                        num1++;
                    }
                }
                if(isOperator(temp)==true){
                    operatorNum++;
                }
            }
            //如果式子里没有运算符，就返回式子本身的值
            if(operatorNum==0){
                return str;
            }
            //每个数字不能有多个小数点
            for(var i=0;i<Strnumbers.length;i++){
                var dotcount=0;
                for(var j=0;j<Strnumbers[i].length;j++){
                    if(Strnumbers[i].charAt(j)=="."){
                        dotcount++;
                    }
                }
                if(dotcount>1){
                    return "Error!";
                }
            }
            //数字总会比操作符+-*/多一个
            if(numbers.length==(operators.length+1)){
                for(var i=0;i<numbers.length;i++){
                    //每个数字的开头和结尾不可以是小数点
                    if(numbers[i].indexOf(".")==0||numbers[i].indexOf(".")==(numbers[i].length-1)){
                        return "Error!"
                    }
                    //不能出现类似0123这样的数字
                    if(numbers[i].indexOf(".")==-1){
                        if(numbers[i].charAt(0)=="0"&&numbers[i].length>1){
                            return "Error!";
                        }
                    }
                }
                //除号后面不能为0或者0.00之类的小数
                for(var i=0;i<operators.length;i++){
                    if(operators[i]=="÷"){
                        //判断除号后面的数字是否除了0或者小数点不包含其他数字
                        var testnum=numbers[i+1].toString();
                        var flag1=testnum.indexOf("1");
                        var flag2=testnum.indexOf("2");
                        var flag3=testnum.indexOf("3");
                        var flag4=testnum.indexOf("4");
                        var flag5=testnum.indexOf("5");
                        var flag6=testnum.indexOf("6");
                        var flag7=testnum.indexOf("7");
                        var flag8=testnum.indexOf("8");
                        var flag9=testnum.indexOf("9");
                        if(flag1==-1&&flag2==-1&&flag3==-1&&flag4==-1&&flag5==-1&&flag6==-1&&flag7==-1&&flag8==-1&&flag9==-1){
                            return "Error!";
                        }
                    }
                }
            }else{
                return "Error!";
            }
            //将字符形式的numbers数组转换成浮点数组
            for(var i=0;i<numbers.length;i++){
                Strnumbers[i]=numbers[i];
                numbers[i]=parseFloat(numbers[i]);
            }
            for(var i=0;i<operators.length;i++){
                if(operators[i]=="×"){
                    if(i==0){
                        replaceNums[num2]=numbers[i]*numbers[i+1];
                        replaceStrings[num2]=(Strnumbers[i]+"×");
                    }
                    if(i>0){
                        if(operators[i-1]=="+"||operators[i-1]=="-"){
                            replaceNums[num2]=numbers[i]*numbers[i+1];
                            replaceStrings[num2]=(Strnumbers[i]+"×");
                        }
                        else{
                            replaceNums[num2]*=numbers[i+1];
                            replaceStrings[num2]+=(Strnumbers[i]+"×");
                        }
                    }
                    if(i==operators.length-1){
                        replaceStrings[num2]+=Strnumbers[i+1];
                    }
                }
                if(operators[i]=="÷"){
                    if(i==0){
                        replaceNums[num2]=numbers[i]/numbers[i+1];
                        replaceStrings[num2]=(Strnumbers[i]+"÷");
                    }
                    if(i>0){
                        if(operators[i-1]=="+"||operators[i-1]=="-"){
                            replaceNums[num2]=numbers[i]/numbers[i+1];
                            replaceStrings[num2]=(Strnumbers[i]+"÷");
                        }
                        else{
                            replaceNums[num2]/=numbers[i+1];
                            replaceStrings[num2]+=(Strnumbers[i]+"÷");
                        }
                    }
                    if(i==operators.length-1){
                        replaceStrings[num2]+=Strnumbers[i+1];
                    } 
                }
                if(operators[i]=="+"){
                    if(i>0){
                        if(operators[i-1]=="×"||operators[i-1]=="÷"){
                            replaceStrings[num2]+=Strnumbers[i];
                            num2++;
                        }
                    }
                }
                if(operators[i]=="-"){
                    if(i>0){
                        if(operators[i-1]=="×"||operators[i-1]=="÷"){
                            replaceStrings[num2]+=Strnumbers[i];
                            num2++;
                        }
                    }
                }
            }
            //将算出的乘除结果替换
            for(var i=0;i<replaceStrings.length;i++){
                str=str.replace(replaceStrings[i],String(replaceNums[i]));
            }
            //再次分割
            for(var i=0;i<str.length;i++){
                temp=str.charAt(i);
                if(i==0){
                    numbers1[num3]=temp;
                }
                if(i>0){
                    var previousChar=str.charAt(i-1);
                    if(isOperator(previousChar)==true){
                        numbers1[num3]=temp;
                    }else{
                        if(isNum(temp)==true||temp=="."){
                            numbers1[num3]+=temp;
                        }
                    }
                    if(isOperator(temp)==true){
                        operators1[num3]=temp;
                        num3++;
                    }
                }
            }
            for(var i=0;i<numbers1.length;i++){
                numbers1[i]=parseFloat(numbers1[i]);
            }
            result=numbers1[0];
            for(var i=0;i<operators1.length;i++){
                if(operators1[i]=="+"){
                    result+=numbers1[i+1];
                }
                if(operators1[i]=="-"){
                    result-=numbers1[i+1];
                }
            }
            //精度处理
            var m=Math.pow(10,4);
            result=parseInt(result*m)/m;
            return String(result);
        }
        //定义记录一条式子有多少个运算符的方法
        function OperatorCount(str){
            var operatorCount=0;
            for(var i=0;i<str.length;i++){
                if(isOperator(str.charAt(i))==true){
                    operatorCount++;
                }
            }
            return operatorCount;
        }
        //定义带括号的运算方法
        function outercaculate(str){
            var index=0;
            var innerStr="";
            var innerResult=0;
            var count1=0;
            var count2=0;
            var temp="";
            for(var i=0;i<str.length;i++){
                temp=str.charAt(i);
                if(temp=="("){
                    count1++;
                }
                if(temp==")"){
                    count2++;
                }
            }
            //"("和")"数量要一样
            if(count1!=count2){
                return "Error!";
            }
            while(str.indexOf("(")>-1){
                index=str.indexOf(")",str.lastIndexOf("(")+1);
                innerStr=str.substring(str.lastIndexOf("(")+1,index);
                if(OperatorCount("("+innerStr+")")==0){
                    return "Error!";
                }
                innerResult=innercaculate(innerStr);
                if(innerResult=="Error!"){
                    return "Error!";
                }
                str=str.replace("("+innerStr+")",String(innerResult));
            }
            result=innercaculate(str);
            return String(result);
        }
        function Caculate(){
            var suanshi=document.getElementById("算式").innerHTML;
            var finalResult=outercaculate(suanshi);
            var textResult=document.getElementById("结果");
            textResult.innerHTML=finalResult;
        }
        //判断是否为运算符
        function isOperator(x){
            var operatorCheck="+-×÷";
            return operatorCheck.indexOf(String(x))>-1;
        }
        //判断是否为数字
        function isNum(x){
            var numCheck="1234567890";
            return numCheck.indexOf(String(x))>-1;
        }
        function clearAll(){
            document.getElementById("算式").innerHTML="";
            document.getElementById("结果").innerHTML="";
        }
        function Delete(){
            document.getElementById("算式").innerHTML=document.getElementById("算式").innerHTML.substring(0,document.getElementById("算式").innerHTML.length-1);
        }
        // function NumAfterDotCount(str){
        //     var count=0;
        //     for(var i=0;i<str.length;i++){
        //         if(str.charAt(i)=="."){
        //             for(var j=1;j<=str.length-i;j++){
        //                 if(isNum(str.charAt(i+j))==true){
        //                     count++;
        //                 }
        //             }
        //             return count;
        //         }
        //     }
        //     return 0;
        // }